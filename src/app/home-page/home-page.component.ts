import { Component, OnInit } from '@angular/core';
import { trigger, transition, animate, style } from '@angular/animations';
import { Subscription } from 'rxjs';
import { TraduzioniService } from 'src/services/Traduzioni.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
  animations:[
    trigger('fade', [
      transition('void => *', [style({ opacity: 0 }), animate('300ms', style({ opacity: 1 }))]),
      transition('* => void', [style({ opacity: 1 }), animate('300ms', style({ opacity: 0 }))]),
    ])
  ]
})
export class HomePageComponent implements OnInit {
  showSideBar = false;
  openMenu = false;
  contatoreStepper = 0;
  timerReset;
  testoDolomiti: string;
  testoPalma: string;
  testoPillar: string;
  testoPonte: string;
  showPlayButton = false;
  profilo: string;
  lingue: string;
  linguaSelezionata = 'ita';
  immagine = [
    '../../assets/images/Dolomiti.jpg',
    '../../assets/images/Palma.jpg',
    '../../assets/images/Pillar.jpg',
    '../../assets/images/Ponte.jpg'
  ]
  constructor(private servizioTraduzioni: TraduzioniService, private route: Router) { }

  ngOnInit() {
    this.timerReset = setInterval(() => {
      this.contatoreStepper = ++this.contatoreStepper % this.immagine.length;
    }, 10000
    )
    this.aggiornaTraduzioni('ita');
  }
  mostraSideBar() {
    this.showSideBar = !this.showSideBar;
  }
  apriMenu() {
    this.openMenu = !this.openMenu;
  }
  imgPrecedente() {
    if (this.contatoreStepper == 0) {
      this.contatoreStepper = this.immagine.length - 1;
    } else {
      this.contatoreStepper = this.contatoreStepper - 1;
    }
    this.resetInterval();
  }
  prossimaImg() {
    if (this.contatoreStepper >= this.immagine.length - 1) {
      this.contatoreStepper = 0;
    } else {
      this.contatoreStepper = this.contatoreStepper + 1;
    }
    this.resetInterval();
  }
  resetInterval() {
    clearInterval(this.timerReset);
    this.timerReset = setInterval(() => {
      this.contatoreStepper = ++this.contatoreStepper % this.immagine.length;
    }, 10000
    )
  }
  stopInterval() {
    clearInterval(this.timerReset);
    this.showPlayButton = !this.showPlayButton;
  }
  addInterval() {
    this.contatoreStepper = ++this.contatoreStepper % this.immagine.length;
    this.timerReset = setInterval(() => {
      this.contatoreStepper = ++this.contatoreStepper % this.immagine.length;
    }, 10000
    )
    this.showPlayButton = !this.showPlayButton;
  }
  clickTranslationFromSideBar() {

  }

  clickTranslation() {
    this.mostraSideBar();
    this.clickTranslationFromSideBar()
  }
  redirectToLogin() {
  }
  aggiornaTraduzioni(lingua) {
    this.linguaSelezionata = lingua;
    let foglio;
    if (lingua == 'ita') {
      this.servizioTraduzioni.getItaliano().subscribe(
        (result) => {
          foglio = result;
          this.profilo = foglio.Profilo;
          this.lingue = foglio.Lingue;
          this.testoPillar = foglio.TestoAquileia;
          this.testoPonte = foglio.TestoCividale;
        }, (error) => {
          console.log(error);
        }
      )
      
      
    } else if (lingua == 'eng') {
      this.servizioTraduzioni.getInglese().subscribe(
        (result) => {
          foglio = result;
          this.profilo = foglio.Profilo;
          this.lingue = foglio.Lingue;
          this.testoPillar = foglio.TestoAquileia;
          this.testoPonte = foglio.TestoCividale;
        }, (error) => {
          console.log(error);
        }
      )

    }else if (lingua == 'ger') {
      this.servizioTraduzioni.getTedesco().subscribe(
        (result) => {
          foglio = result;
          this.profilo = foglio.Profilo;
          this.lingue = foglio.Lingue;
          this.testoPillar = foglio.TestoAquileia;
          this.testoPonte = foglio.TestoCividale;
        }, (error) => {
          console.log(error);
        }
      )
      
    }else {
      this.servizioTraduzioni.getSloveno().subscribe(
        (result) => {
          foglio = result;
          this.profilo = foglio.Profilo;
          this.lingue = foglio.Lingue;
          this.testoPillar = foglio.TestoAquileia;
          this.testoPonte = foglio.TestoCividale;
          
        }, (error) => {
          console.log(error);
        }
      )
      
    }
  }
}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AquileiaComponent } from './aquileia.component';

describe('AquileiaComponent', () => {
  let component: AquileiaComponent;
  let fixture: ComponentFixture<AquileiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AquileiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AquileiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

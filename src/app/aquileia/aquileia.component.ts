import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-aquileia',
  templateUrl: './aquileia.component.html',
  styleUrls: ['./aquileia.component.css']
})
export class AquileiaComponent implements OnInit {

  // this.router.navigate(['/'nome componente''])
  // il nome componente lo recuperi dal app-routing-module
  constructor(private router: Router) { }

  ngOnInit() {
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomePageComponent } from './home-page/home-page.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { TraduzioniService } from "../services/Traduzioni.service";
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UtenzeService } from "../services/utenze.service";
import { FooterComponent } from './footer/footer.component';
import { AquileiaComponent } from './aquileia/aquileia.component';
@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    LoginComponent,
    FooterComponent,
    AquileiaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AngularFontAwesomeModule
  ],
  providers: [
    TraduzioniService,
    UtenzeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TraduzioniService } from 'src/services/Traduzioni.service';
import { UtenzeService } from 'src/services/utenze.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  reactiveForm: FormGroup;
  showPage = false;
  errorMessage = false;
  invia: string;
  errorMessageText: string;
  lingua: string;
  registrazione: string;
  passwordDimenticata: string;
  Pulsante: string;
  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute,private servizioTraduzioni: TraduzioniService, private servizioUtenti: UtenzeService, private router: Router) {
    this.route.paramMap.subscribe( paramMap => {
      this.lingua = paramMap.get('lingua');
      this.aggiornaTraduzioni(this.lingua);
  })
    this.reactiveForm = this.formBuilder.group({
      Username: ['',
        [
      ]
      ],
      password: ['',
           [
           ]
      ]
      })
   }

  ngOnInit() {
  }

  getusernameControl() {
    return this.reactiveForm.value.Username;
  }

  getpasswordControl() {
    return this.reactiveForm.value.password;
  }
  submitForm() {
    this.servizioUtenti.getByUsernamePassword(this.getusernameControl(), this.getpasswordControl()).subscribe(
      (res) => {
        if (res.length > 0) {
          sessionStorage.setItem('id', res[0].id)
          this.router.navigate(['/Dashboard'])
        } else {
          this.errorMessage = true;
        }
      }, (error) => {
        console.log(error)
      }
    )
  }
  
  aggiornaTraduzioni(lingua) {
    let foglio;
    if (lingua == 'ita') {
      this.servizioTraduzioni.getItaliano().subscribe(
        (result) => {
          foglio = result;
          this.traduzione(foglio);
          console.log(foglio)
        }, (error) => {
          console.log(error);
        }
      )
      
      
    } else if (lingua == 'eng') {
      this.servizioTraduzioni.getInglese().subscribe(
        (result) => {
          foglio = result;
          this.traduzione(foglio);
        }, (error) => {
          console.log(error);
        }
      )

    }else if (lingua == 'ger') {
      this.servizioTraduzioni.getTedesco().subscribe(
        (result) => {
          foglio = result;
          this.traduzione(foglio);
        }, (error) => {
          console.log(error);
        }
      )
      
    }else {
      this.servizioTraduzioni.getSloveno().subscribe(
        (result) => {
          foglio = result;
          this.traduzione(foglio);
        }, (error) => {
          console.log(error);
        }
      )
      
    }
  }
  traduzione(foglio) {
    this.invia = foglio.invia
    this.errorMessageText = foglio.errorMessageText;
    this.registrazione = foglio.registrazione;
    this.passwordDimenticata = foglio.passwordDimenticata;
    this.showPage = true;
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomePageComponent } from './home-page/home-page.component';
import { AquileiaComponent } from "./aquileia/aquileia.component";

const routes: Routes = [
  { path: 'Dashboard', component: HomePageComponent },
  { path: 'Login/:lingua', component: LoginComponent },
  { path: 'Aquileia', component: AquileiaComponent},
  { path: '', redirectTo: 'Dashboard', pathMatch: 'full' },
  { path: '**', redirectTo: 'Dashboard'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

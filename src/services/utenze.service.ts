import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
  })
export class UtenzeService {
    baseUrl = '';
    constructor(private http: HttpClient) {
        this.baseUrl = 'http://localhost:4000'
    }
    getByUsernamePassword(username: string, password: string) {
        return this.http.get<any[]>(this.baseUrl + '/users?username=' + username + '&password=' + password);
    }
}
import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
  })
export class TraduzioniService {
    baseUrl = '';
    constructor(private http: HttpClient) {
        this.baseUrl = 'http://localhost:3000'
    }

  getItaliano(): Observable<any[]> {
    return this.http.get<any[]>(this.baseUrl + '/TestoItaliano');
  }
  getInglese(): Observable<any[]> {
    return this.http.get<any[]>(this.baseUrl + '/TestoInglese');
  }
  getTedesco(): Observable<any[]> {
    return this.http.get<any[]>(this.baseUrl + '/TestoTedesco');
  }
  getSloveno(): Observable<any[]> {
    return this.http.get<any[]>(this.baseUrl + '/TestoSloveno');
  }
}